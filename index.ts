import * as mongoose from 'mongoose';
import {Retailer} from './retailerSchema'

(async () =>{
    await mongoose.connect('mongodb://localhost:27017/testdb', {useNewUrlParser: true});
    const sample_retailer = new Retailer({Company_name: "Microsoft", City: "REDMOND", Retailer_ID: "Micro101", password: "Satya@1967"});
    const sample = await sample_retailer.save();
    console.log(sample);
})();