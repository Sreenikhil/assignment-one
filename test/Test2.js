const mongoose = require('mongoose');
const assert = require('chai').assert;
const Medicine = require('../medicineSchema').Medicine;


// Test Case

const sample_medicine = new Medicine({Medicine_name: "Tylenol", count: 100, Medicine_ID: 101 });
const sample_medicine1 = new Medicine({Medicine_name: "Paracetamol", count: 50, Medicine_ID: 102 });


before(function (done) {
    const db_connect =  mongoose.connect("mongodb://localhost:27017/testdb", {useNewUrlParser: true} );
    const db = mongoose.connection;
    db.on('error', (error)=>{
        done(error)
    });
    db.once('open', function () {
        console.log("We are connected to the database");
        done()
    })
});

// ------------------------- Count greater than 0 ------------------------------

describe("count greater than 0", function () {
    it("count grater than 0", function () {
        let p = sample_medicine.count;
        assert.isAbove(p,0)
    })
});

// ---------------------------- Save to database -----------------------------------------

describe("save to database", function () {
    it("database save", async function () {
        await sample_medicine.save();
    })
});

// ---------------------- Medicine is unique ------------------------------------

describe("unique medicine", function () {
    it("unique medicine", async function () {
        const t =  await Medicine.find({Medicine_ID: sample_medicine1.Medicine_ID});
        const h = t.length;
        assert.equal(h,0)
    })
});

// -------------------- Length of medicine id is greater than 3 --------------------------

describe("length of medicine id", function () {
    it("length of medicine", function (){
        const value = sample_medicine.Medicine_ID;
        assert.isTrue(value > 0 && value < 9999999 );
    })

});

// --------------------- Count is a number ---------------------------

describe("Count", function () {
    it("Count is a number", function () {
        const count = sample_medicine.count;
        assert.isNotNaN(count)
    })
});

// --------------------------- All required fields are give --------------------------------------

describe("all fields given", function () {
    it("all fields given", function () {
        function checknullorundefined(p){
            return p === null || p === undefined;
        }
        let case1 = checknullorundefined(sample_medicine.Medicine_name);
        let case2 = checknullorundefined(sample_medicine.Medicine_ID);
        let case3 = checknullorundefined(sample_medicine.count);

        assert.isFalse(case1 || case2 || case3)
    })
});

// --------------- Check if medicine id is a string ---------------------------------

describe("Medicine Id", function () {
    it("Medicine id should be a string", function () {
        assert.isString(sample_medicine.Medicine_ID)
    })
});


// --------------------------- Medicine name length greater than 1 -------------------

describe("medicine-name", function () {
   it("medicine-name", function () {
       let k = sample_medicine.Medicine_name.length;
       assert.isAbove(k, 2)
   })
});

// --------------------------------------------------------------------------------------

after(async function(){
    try {
        await mongoose.connection.db.dropDatabase();
    }
    catch (e) {
        console.log(e)
    }
});

// --------------------------------------------------------------------------------------
