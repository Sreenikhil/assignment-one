/*
Custom validation is declared by passing a validation function.
 */
import * as mongoose from 'mongoose';
import  {IRetailer} from './Retailer'
const Schema = mongoose.Schema;


const retailerSchema : mongoose.Schema<IRetailer> = new mongoose.Schema<IRetailer>({
    Company_name: {
        type: String,
        required: true,  // Built-In validation
        get: obfuscate,
    },
    Country_code:{
        type: Number,
        required: true  // Built-In validation
    },

    Phone_Number:{
        type: Number,
        required: true
    },

    Address:{
        type: String,
        required: true
    },

    Country:{
        type: String,
        required: true
    },

    City: {
        type: String,
        validate: {
            validator: function (city: string): boolean {
                return city.length > 1
            }
        }
    },

    // Should have at least length of 4 characters
    Retailer_ID: {
        type: String,
        validate: {
            validator: function (id: string): boolean {
                return id.length >= 4
            },
            message: "retailer id should have a minimum length of 4"
        },
        required: true,
        unique: true,
        set: convert_lower
    },



    // Password has lot of requirements. Should have an uppercase, lowercase, number and special characters.
    password: {
        type: String,
        validate: {
            validator: function (password: string): boolean {
                let re = /[a-z]/;
                let re1 = /[A-Z]/;
                let re2 = /[0-9]/;
                let re3 = /[!@#$%^&*]/;
                let t1 = re.test(password);
                let t2 = re1.test(password);
                let t3 = re2.test(password);
                let t4 = re3.test(password);
                return t1 && t2 && t3 && t4 && password.length > 8
            },
            message: "Password should have length of atleast 8, one uppercase, one lowercase and one number at least"
        },
        required: true

    }
});

// Getter and Setter
retailerSchema.virtual('full_Phone_number')
    .get(function (this: IRetailer) {
        return "+" + this.Country_code+ ""+ this.Phone_Number
    }).set(function (this: IRetailer, full_Phone_number: string) {
    const [countrycode, phonenumber]: string[] = full_Phone_number.split("");
    this.Country_code = parseInt(countrycode, 10)
});



// Setter: Making retailer_id to lowercase

function convert_lower(retailer_id){
    if(!(this instanceof mongoose.Document)){
        return retailer_id
    }
    return retailer_id.toLowerCase();
}

// Getter: Obfuscating password

function obfuscate(company_name) {
    if(company_name) {
        return "****";
    }
}




export const Retailer = mongoose.model<IRetailer>('Retailer', retailerSchema);




