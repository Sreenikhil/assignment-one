// I am gonna do a crazy job
// Build an express app which has a timestamp middleware and Logging middleware


import express, {NextFunction, Request, Response, Application} from 'express'

const app = express();

// Login Middleware

const logmiddleware = (req: any, res: any, next: any) => {
    console.log("Logged");
    next()
};

// Request time middleware

const requestTime = (req: any, res: any, next:any) =>{
     req.time = Date.now();
    next()
};

// Goal: To create a third party middleware
//Okta has an Express middleware for OIDC security that I’ll show you to demonstrate using third-party middleware libraries.
/*const oidc = new ExpressOIDC({
    issuer: 'https://{yourOktaDomain}/oauth2/default',
    client_id: '{yourClientId}',
    client_secret: '{yourClientSecret}',
    redirect_uri: 'http://localhost:3000/authorization-code/callback',
    scope: 'openid profile'
});*/


// Just writing add function in typescript

const add = (a: number, b: number): number => a+b;

// We are making a cookie

const createCookie = (req: Request, res: Response, next:NextFunction) => {
    res.cookie("Oreo", "I am a cookie");
       next()
};

app.use(logmiddleware, requestTime, createCookie);

app.get("/" , (req: any, res: any) =>{
     console.log("I am pinged");
     console.log(req.time);
    console.log(add(8,2))
     console.log(req.cookie)
});

app.listen(5000, ()=>{console.log("I am listening to the port 5000")});